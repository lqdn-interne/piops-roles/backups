Backups
=========

Ce rôle se charge d'orchestrer les sauvegardes des services de La Quadrature Du Net.


Requirements
------------

N/A

Role Variables
--------------

```
dump_location: "/var/backup/"
```
Où sauvegarder les dumps des bases de données

```
sysadmin_mail: "root@localhost"
```
Email de l'administration responsable

```
db_backup_user: root
```
Utilisateurice UNIX pour la réalisation des sauvegardes

```
db_backup_group: root
```
Groupe UNIX l'utilisateurice UNIX pour la réalisation des sauvegardes

Dependencies
------------

N/A

Example Playbook
----------------

    - hosts: servers
      roles:
         - backups

License
-------

GPLv3

Author Information
------------------

Écrit par Nono, pour la Quadrature Du Net
